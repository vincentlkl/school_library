class CreateLoanBooks < ActiveRecord::Migration[5.2]
  def change
    create_table :loan_books do |t|
      t.references :borrower, index: true
      t.references :return, index: true
      t.references :loan, index: true
      t.references :book, index: true
      t.date :loan_date
      t.date :due_date
      t.date :return_date
      t.integer :day, default: 0
      t.integer :late_day, default: 0
      t.string :status, default: "loan"
      t.timestamps
    end
  end
end
