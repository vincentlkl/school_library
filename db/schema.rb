# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_06_12_231401) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "books", force: :cascade do |t|
    t.string "name", null: false
    t.string "year"
    t.string "author"
    t.string "genre"
    t.string "barcode"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "price_cents", default: 0, null: false
    t.string "price_currency", default: "USD", null: false
    t.string "status", default: "active"
    t.string "code"
    t.string "language"
    t.string "rack"
  end

  create_table "borrowers", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.string "year"
    t.text "remark"
    t.string "role", default: "student"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "school_class"
  end

  create_table "loan_books", force: :cascade do |t|
    t.bigint "borrower_id"
    t.bigint "return_id"
    t.bigint "loan_id"
    t.bigint "book_id"
    t.date "loan_date"
    t.date "due_date"
    t.date "return_date"
    t.integer "day", default: 0
    t.integer "late_day", default: 0
    t.string "status", default: "loan"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "year"
    t.string "school_class"
    t.string "borrower_role"
    t.boolean "is_late_paid", default: false
    t.date "late_paid_date"
    t.integer "late_paid_amount_cents", default: 0, null: false
    t.string "late_paid_amount_currency", default: "USD", null: false
    t.index ["book_id"], name: "index_loan_books_on_book_id"
    t.index ["borrower_id"], name: "index_loan_books_on_borrower_id"
    t.index ["loan_id"], name: "index_loan_books_on_loan_id"
    t.index ["return_id"], name: "index_loan_books_on_return_id"
  end

  create_table "loans", force: :cascade do |t|
    t.bigint "borrower_id"
    t.date "loan_date"
    t.date "due_date"
    t.integer "day", default: 0
    t.text "remark"
    t.integer "total_quantity"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "year"
    t.string "school_class"
    t.string "borrower_role"
    t.index ["borrower_id"], name: "index_loans_on_borrower_id"
  end

  create_table "penalty_costs", force: :cascade do |t|
    t.integer "days"
    t.integer "penalty_cost_cents", default: 0, null: false
    t.string "penalty_cost_currency", default: "USD", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "returns", force: :cascade do |t|
    t.bigint "borrower_id"
    t.date "return_date"
    t.text "remark"
    t.integer "total_quantity"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["borrower_id"], name: "index_returns_on_borrower_id"
  end

  create_table "students", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.string "year"
    t.text "remark"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "teachers", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "username"
    t.string "full_name"
    t.string "encrypted_password", default: "", null: false
    t.string "role", default: "student", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "write_off_books", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "write_off_id"
    t.bigint "book_id"
    t.integer "price_cents", default: 0, null: false
    t.string "price_currency", default: "USD", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["book_id"], name: "index_write_off_books_on_book_id"
    t.index ["user_id"], name: "index_write_off_books_on_user_id"
    t.index ["write_off_id"], name: "index_write_off_books_on_write_off_id"
  end

  create_table "write_offs", force: :cascade do |t|
    t.bigint "user_id"
    t.date "write_off_date"
    t.text "remark"
    t.integer "total_quantity"
    t.integer "total_price_cents", default: 0, null: false
    t.string "total_price_currency", default: "USD", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_write_offs_on_user_id"
  end

end
