class Book < ApplicationRecord
  include AASM
  monetize :price_cents, numericality: { greater_than_or_equal_to: 0 }

  aasm column: :status do
    state :active, initial: true
    state :inactive
  end

  class << self
    def import(file)
      spreadsheet = open_spreadsheet(file)
      header = spreadsheet.row(1).collect{|x| x.downcase.strip}
      (2..spreadsheet.last_row).map do |i|
        row = Hash[[header, spreadsheet.row(i)].transpose]
        book = Book.where(barcode: row["barcode"]).first_or_initialize
        book.update(row)
      end
    end

    def open_spreadsheet(file)
      case File.extname(file.original_filename)
      when ".xlsx" then Roo::Excelx.new(file.path)
      else raise "Unknown file type: #{file.original_filename}"
      end
    end
  end
end
