class AddIsLatePaidToLoanBooks < ActiveRecord::Migration[5.2]
  def change
    add_column :loan_books, :is_late_paid, :boolean, default: false
    add_column :loan_books, :late_paid_date, :date
    add_monetize :loan_books, :late_paid_amount
  end
end
