class AddRackToBooks < ActiveRecord::Migration[5.2]
  def change
    add_column :books, :rack, :string
  end
end
