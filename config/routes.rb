Rails.application.routes.draw do
  devise_for :users

  namespace :users do
    resources :admins
    resources :students do
      collection do
        post :import
      end
    end
    resources :teachers do
      collection do
        post :import
      end
    end
  end
  
  namespace :settings do
    resource :penalty_costs
    resource :backups
  end

  resources :borrowers do
    collection do 
      get :ajax_select2
    end
  end

  resources :books do
    collection do
      post :import
    end
  end
  
  resources :returns do
    collection do
      post :add_book
    end
  end
  
  resources :loans do 
    collection do
      post :add_book
    end
  end

  resources :write_offs do
    collection do
      post :add_book
    end
  end

  resources :late_returns

  namespace :reports do
    resources :loans do
      collection do
        get :top_rank
        get :by_year
        get :by_class
        get :detail
        post :detail
      end
    end
  end

  root "users/students#index"
end