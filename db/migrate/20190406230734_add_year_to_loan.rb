class AddYearToLoan < ActiveRecord::Migration[5.2]
  def change
    add_column :loans, :year, :string
    add_column :loans, :school_class, :string
    add_column :loans, :borrower_role, :string
  end
end
