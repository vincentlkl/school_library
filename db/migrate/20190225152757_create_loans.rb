class CreateLoans < ActiveRecord::Migration[5.2]
  def change
    create_table :loans do |t|
      t.references :borrower, index: true
      t.date :loan_date
      t.date :due_date
      t.integer :day, default: 0
      t.text :remark
      t.integer :total_quantity
      t.timestamps
    end
  end
end
