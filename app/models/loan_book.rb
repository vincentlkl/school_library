class LoanBook < ApplicationRecord
  include AASM
  monetize :late_paid_amount_cents, numericality: { greater_than_or_equal_to: 0 }

  belongs_to :borrower
  belongs_to :book
  belongs_to :loan  
  belongs_to :return, optional: true

  scope :overdue, -> { where("due_date < ? AND (return_date IS NULL OR return_date > due_date) AND is_late_paid = ?", Time.zone.today, false) }

  aasm column: :status do
    state :loan, initial: true
    state :returned
    state :lost

    event :return do
      transitions from: :loan, to: :returned, success: :confirm_return
    end
  end

  def confirm_return
    return_date = Time.zone.today
    late_day = (return_date - due_date).to_i

    self.update_columns(
      return_date: return_date,
      late_day: late_day
    )
  end

  def display_late_day
    late_day < 0 ? 0 : late_day
  end

  def calculate_late_days
    late_day.zero? ? (Time.zone.today - due_date).to_i : late_day rescue 0
  end

  def calculate_late_return_cost
    (calculate_late_days * PenaltyCost.first.penalty_cost) rescue 0
  end
end