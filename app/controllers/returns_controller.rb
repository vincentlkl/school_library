class ReturnsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_return, only: %I[show]

  def index
    respond_to do |format|
      format.html
      format.json { render json: ReturnsDatatable.new(view_context) }
    end
  end

  def new
    @return = Return.new
    @return.return_date = Time.zone.today
  end

  def show; end

  def create
    @return = Return.new(return_params)
    if @return.save
      set_return_books
      flash[:success] = "This return has been created."
      check_redirection      
    else
      flash[:error] = "Sorry unable to create book due to #{@return.errors.full_messages.to_sentence}"
      render :new
    end
  end

  def add_book
    @book = Book.find_by(barcode: params[:barcode])
    @loan_book = LoanBook.find_by(book_id: @book&.id, status: "loan")
  end

  private

  def return_params
    params.require(:return).permit([:borrower_id, :return_date, :remark])
  end

  def set_return
    @return = Return.find(params[:id])
  end

  def set_return_books
    loan_book_ids = params["return"]["loan_books_attributes"].keys
    loan_books = LoanBook.where(id: loan_book_ids, status: "loan")
    loan_books.update_all(return_id: @return.id)
    loan_books.each{|x| x.return! if x.may_return? }
  end

  def check_redirection
    redirect_to return_path(@return)
  end
end
