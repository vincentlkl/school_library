$ ->
  $('.ajax-datatable').dataTable
    sPaginationType: "full_numbers"
    bProcessing: true
    bServerSide: true
    order: [[ 0, "desc" ]]
    lengthMenu: [[10, 25, 50, 100, 10000], [10, 25, 50, 100, 10000]]
    sAjaxSource: $('.ajax-datatable').data('source')
  
  $('.datatable').dataTable()