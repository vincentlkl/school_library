class AddSchoolClassToBorrower < ActiveRecord::Migration[5.2]
  def change
    add_column :borrowers, :school_class, :string
    add_column :loan_books, :year, :string
    add_column :loan_books, :school_class, :string
    add_column :loan_books, :borrower_role, :string
  end
end
