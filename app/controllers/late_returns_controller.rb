class LateReturnsController < ApplicationController
  before_action :authenticate_user!
  
  def index
    @results = LoanBook.select(:id, :return_date, :loan_date, :late_day, :due_date, :status, :school_class, :book_id, :is_late_paid, :borrower_id).overdue.where(is_late_paid: false).order(:borrower_id, :due_date)
  end

  def show
    @borrower = Borrower.find(params[:id])
    @results = @borrower.loan_books.overdue.where(is_late_paid: false).order(:due_date)
  end

  def update
    @borrower = Borrower.find(params[:id])

    if params[:loan_book_ids].blank?
      flash[:error] = "No item was selected."
    else
      LoanBook.where(id: params[:loan_book_ids]).each do |loan_book|
        loan_book.update(
          is_late_paid: true,
          late_paid_date: Time.zone.today,
          late_paid_amount: loan_book.calculate_late_return_cost
        )
      end
      flash[:success] = "Penalty is paid."   
    end
    redirect_to late_return_path(@borrower.id)
  end
end
