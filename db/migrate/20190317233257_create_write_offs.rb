class CreateWriteOffs < ActiveRecord::Migration[5.2]
  def change
    create_table :write_offs do |t|
      t.references :user, index: true
      t.date :write_off_date
      t.text :remark
      t.integer :total_quantity
      t.monetize :total_price
      t.timestamps
    end
  end
end
