class CreateBooks < ActiveRecord::Migration[5.2]
  def change
    create_table :books do |t|
      t.string :name, null: false
      t.string :year
      t.string :author
      t.string :genre
      t.string :year
      t.string :barcode, unique: true
      t.text :description
      t.timestamps
    end
  end
end
