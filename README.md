# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...


$ docker-compose run web rails new . --force --no-deps --database=postgresql

$ docker-compose build

$ docker-compose up

$ docker-compose run web rake db:create

$ docker-compose run web rake db:migrate

$ docker-compose run web rails db:pull

$ docker ps

$ docker-machine ip 8a1fb16f4833