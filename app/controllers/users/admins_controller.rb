class Users::AdminsController < Users::BaseController
  before_action :set_user, only: %I[edit update]

  def index
    @users = User.all
  end

  def new
    @user = User.new
  end

  def edit; end

  def create
    @user = User.new(user_params)
    if @user.save
      flash[:success] = "This users has been created."
      redirect_to users_admins_path
    else
      flash[:error] = "Sorry unable to create user due to #{@user.errors.full_messages.to_sentence}"
      render :new
    end
  end

  def update
    if @user.update(user_params)
      flash[:success] = "This users has been updated."
      redirect_to users_admins_path
    else
      flash[:error] = "Sorry unable to update user due to #{@user.errors.full_messages.to_sentence}"
      render :new
    end
  end

  private

  def user_params
    params.require(:user).permit(%I[username email password password_confirmation])
  end

  def set_user
    @user = User.find(params[:id])
  end
end
