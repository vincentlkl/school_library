class Users::TeachersController < Users::BaseController
  before_action :set_teacher, only: %I[edit update]

  def index
    respond_to do |format|
      format.html
      format.json { render json: TeachersDatatable.new(view_context) }
      format.xlsx { @teachers = Borrower.teachers }
    end
  end

  def new
    @teacher = Borrower.new
  end

  def edit; end

  def create
    @teacher = Borrower.new(borrower_params)
    @teacher.role = "teacher"
    if @teacher.save
      flash[:success] = "This teachers has been created."
      redirect_to users_teachers_path
    else
      flash[:error] = "Sorry unable to create teacher due to #{@teacher.errors.full_messages.to_sentence}"
      render :new
    end
  end

  def update
    if @teacher.update(borrower_params)
      flash[:success] = "This teachers has been updated."
      redirect_to users_teachers_path
    else
      flash[:error] = "Sorry unable to update teacher due to #{@teacher.errors.full_messages.to_sentence}"
      render :new
    end
  end

  def import
    @teachers = Borrower.import_teachers(params[:file])
    flash[:success] = "The teachers has been imported."
    redirect_to users_teachers_path
  end

  private

  def borrower_params
    params.require(:borrower).permit(%I[code name])
  end

  def set_teacher
    @teacher = Borrower.teachers.find(params[:id])
  end
end
