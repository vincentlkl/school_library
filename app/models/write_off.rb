class WriteOff < ApplicationRecord
  monetize :total_price_cents, numericality: { greater_than_or_equal_to: 0 }

  belongs_to :user  
  has_many :write_off_books

  accepts_nested_attributes_for :write_off_books, allow_destroy: true
end
