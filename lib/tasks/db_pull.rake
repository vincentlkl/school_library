namespace :db do
  task pull: :environment do
    development_database = Rails.configuration.database_configuration['development']['database']
    system "cd #{Rails.root}"

    system 'bundle exec rake db:drop && bundle exec rake db:create'
    system "pg_restore --verbose --data-only --no-acl --no-owner -h localhost -U postgres -d school_library_development latest.dump"
    system 'bundle exec rails db:environment:set RAILS_ENV=development'
    # system "rm latest.dump"
    User.update_all(device_token: nil)
  end
end