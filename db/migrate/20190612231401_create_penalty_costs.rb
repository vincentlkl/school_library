class CreatePenaltyCosts < ActiveRecord::Migration[5.2]
  def change
    create_table :penalty_costs do |t|
      t.integer :days
      t.monetize :penalty_cost
      t.timestamps
    end
  end
end
