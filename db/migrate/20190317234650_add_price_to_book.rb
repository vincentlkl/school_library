class AddPriceToBook < ActiveRecord::Migration[5.2]
  def change
    add_monetize :books, :price
    add_column :books, :status, :string, default: "active"
  end
end
