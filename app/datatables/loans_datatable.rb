class LoansDatatable
  delegate :params, :h, :link_to, :loan_path, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: Loan.count,
      iTotalDisplayRecords: loans.total_entries,
      aaData: data
    }
  end

private

  def data
    loans.map do |loan|
      [
        loan.id,
        loan&.borrower&.name,
        (loan.loan_date.to_date rescue "-"),
        (loan.due_date.to_date rescue "-"),
        (link_to('Show', loan_path(loan), class: 'btn btn-warning')),
      ]
    end
  end

  def loans
    @loans ||= fetch_loans
  end

  def fetch_loans
    loans = Loan.joins(:borrower).order("#{sort_column} #{sort_direction}")
    loans = loans.page(page).per_page(per_page)
    if params[:sSearch].present?
      loans = loans.where("borrowers.name ilike :search
                        ", search: "%#{params[:sSearch]}%")
    end
    loans.distinct
  end

  def page
    params[:iDisplayStart].to_i/per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = {
      '0': "loans.id",
      '1': "borrowers.name",
      '2': "loans.loan_date",
      '3': "loans.due_date",
      '4': "loans.id",
    }
    columns[params[:iSortCol_0].to_sym]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc" : "asc"
  end
end
