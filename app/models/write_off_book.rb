class WriteOffBook < ApplicationRecord
  monetize :price_cents, numericality: { greater_than_or_equal_to: 0 }

  belongs_to :write_off
  belongs_to :book
  belongs_to :user
end
