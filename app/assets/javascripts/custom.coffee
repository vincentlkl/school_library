$ ->
  $(".ajax-select2").select2
    minimumInputLength: 2
    ajax:
      url: $('.ajax-select2').data("url")
      dataType: 'json'
      multiple: true
      type: 'GET'
      data: (params) ->
        {
          q: params.term
          page: params.page
        }
      processResults: (data, params) ->
        results: data

  $("form input").keydown (event) ->
    if event.keyCode == 13 && !$(this).hasClass("barcode-input")
      event.preventDefault()
      return false

  $("form").on "click", ".remove-tr", (event) ->
    event.preventDefault()
    $(this).closest("tr").remove()


  $(".check-loan-book").on "change", (event) ->
    total = 0
    $(".check-loan-book").each ->
      checked = $(this).is(":checked")
      value = parseFloat($(this).data("value"))
      if checked
        total += value

    $("#total_payment").text(total.toFixed(2))
    
    