class Users::StudentsController < Users::BaseController
  before_action :set_student, only: %I[edit update]

  def index
    respond_to do |format|
      format.html
      format.json { render json: StudentsDatatable.new(view_context) }
      format.xlsx { @students = Borrower.students }
    end
  end

  def new
    @student = Borrower.new
  end

  def edit; end

  def create
    @student = Borrower.new(borrower_params)
    if @student.save
      flash[:success] = "This students has been created."
      redirect_to users_students_path
    else
      flash[:error] = "Sorry unable to create student due to #{@student.errors.full_messages.to_sentence}"
      render :new
    end
  end

  def update
    if @student.update(borrower_params)
      flash[:success] = "This students has been updated."
      redirect_to users_students_path
    else
      flash[:error] = "Sorry unable to update student due to #{@student.errors.full_messages.to_sentence}"
      render :new
    end
  end

  def import
    @students = Borrower.import_students(params[:file])
    flash[:success] = "The students has been imported."
    redirect_to users_students_path
  end

  private

  def borrower_params
    params.require(:borrower).permit(%I[code name year school_class])
  end

  def set_student
    @student = Borrower.students.find(params[:id])
  end
end

