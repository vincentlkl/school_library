class WriteOffsDatatable
  delegate :params, :h, :link_to, :humanized_money, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: Loan.count,
      iTotalDisplayRecords: write_off_books.total_entries,
      aaData: data
    }
  end

private

  def data
    write_off_books.map do |write_off_book|
      [
        write_off_book.id,
        write_off_book.book&.name,
        write_off_book.book&.year,
        humanized_money(write_off_book.book&.price),
        (write_off_book.write_off.created_at.to_date rescue "-"),
      ]
    end
  end

  def write_off_books
    @write_off_books ||= fetch_write_off_books
  end

  def fetch_write_off_books
    write_off_books = WriteOffBook.joins(:book).order("#{sort_column} #{sort_direction}")
    write_off_books = write_off_books.page(page).per_page(per_page)
    if params[:sSearch].present?
      write_off_books = write_off_books.where("borrowers.name ilike :search
                        ", search: "%#{params[:sSearch]}%")
    end
    write_off_books.distinct
  end

  def page
    params[:iDisplayStart].to_i/per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = {
      '0': "write_off_books.id",
      '1': "books.name",
      '2': "write_off_books.write_off_book_date",
      '3': "write_off_books.due_date",
      '4': "write_off_books.id",
    }
    columns[params[:iSortCol_0].to_sym]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc" : "asc"
  end
end