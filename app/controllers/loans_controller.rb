class LoansController < ApplicationController
  before_action :authenticate_user!
  before_action :set_loan, only: %I[show]

  def index
    respond_to do |format|
      format.html
      format.json { render json: LoansDatatable.new(view_context) }
    end
  end

  def new
    @loan = Loan.new
    @loan.loan_date = Time.zone.today
    @loan.day = PenaltyCost.first.days
    @loan.due_date = @loan.loan_date + @loan.day
  end

  def show; end

  def create
    @loan = Loan.new(loan_params)
    set_loan_books
    if @loan.save
      flash[:success] = "This loan has been created."
      redirect_to loan_path(@loan)
    else
      flash[:error] = "Sorry unable to create book due to #{@loan.errors.full_messages.to_sentence}"
      render :new
    end
  end

  def add_book
    @book = Book.find_by(barcode: params[:barcode])
  end

  private

  def loan_params
    params.require(:loan).permit([:borrower_id, :loan_date, :day, :due_date, :remark, loan_books_attributes: [:id, :book_id]])
  end

  def set_loan
    @loan = Loan.find(params[:id])
  end

  def set_loan_books
    @loan.total_quantity = @loan.loan_books.size
    @loan.year = @loan.borrower&.year
    @loan.school_class = @loan.borrower&.school_class
    @loan.borrower_role = @loan.borrower&.role

    @loan.loan_books.each do |loan_book|
      loan_book.borrower_id = @loan.borrower_id
      loan_book.loan_date = @loan.loan_date
      loan_book.due_date = @loan.due_date
      loan_book.day = @loan.day
      loan_book.year = @loan.borrower&.year
      loan_book.school_class = @loan.borrower&.school_class
      loan_book.borrower_role = @loan.borrower&.role
    end
  end
end

