class TeachersDatatable
  delegate :params, :h, :link_to, :edit_users_teacher_path, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: Borrower.teachers.count,
      iTotalDisplayRecords: teachers.total_entries,
      aaData: data
    }
  end

private

  def data
    teachers.map do |teacher|
      [
        teacher.id,
        teacher.code,
        teacher.name,
        (link_to('Edit', edit_users_teacher_path(teacher), class: 'btn btn-warning')),
      ]
    end
  end

  def teachers
    @teachers ||= fetch_teachers
  end

  def fetch_teachers
    teachers = Borrower.teachers.order("#{sort_column} #{sort_direction}")
    teachers = teachers.page(page).per_page(per_page)
    if params[:sSearch].present?
      teachers = teachers.where("borrowers.name ilike :search OR
                        borrowers.code ilike :search
                        ", search: "%#{params[:sSearch]}%")
    end
    teachers.distinct
  end

  def page
    params[:iDisplayStart].to_i/per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = {
      '0': "borrowers.id",
      '1': "borrowers.code",
      '2': "borrowers.name",
      '3': "borrowers.id",
    }
    columns[params[:iSortCol_0].to_sym]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc" : "asc"
  end
end
