class Settings::BackupsController < ApplicationController
  before_action :authenticate_user!

  def create
    system("pg_dump -Fc --no-acl --no-owner -U #{ENV['DATABASE_USER']} #{ENV['DATABASE_NAME']} > latest.dump")
    system("truncate -s 0 log/*.log")
    send_file(
      "#{Rails.root}/latest.dump",
      filename: "#{Time.zone.now.to_s.parameterize}-latest.dump",
    )
  end

end