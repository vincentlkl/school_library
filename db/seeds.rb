unless Rails.env.production?
  if User.none?
    User.create!(email: "admin@demo.com", username: "admin.user", role: "admin", password: "password", password_confirmation: "password")    
  end

  if Book.none?
    (1..10).each do |i|
      Book.create(name: "Book #{1}", year: i, author: "John", barcode: "BOOK#{i}", price: i)
    end
  end

  PenaltyCost.create(days: 7, penalty_cost: 0.1)
end
