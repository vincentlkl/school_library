class Reports::LoansController < ApplicationController
  before_action :authenticate_user!

  def top_rank
    if params[:commit].present?
      start_date = "1-1-#{params[:year]}".to_date
      end_date = "1-#{params[:month]}-#{params[:year]}".to_date.end_of_month
      date_ranges = (start_date..end_date)
      @loan_rank = Loan.where(loan_date: date_ranges).group(:borrower_id).sum(:total_quantity)
    end
  end

  def by_year
    if params[:commit].present?
      start_date = "1-#{params[:month]}-#{params[:year]}".to_date
      end_date = start_date.end_of_month
      date_ranges = (start_date..end_date)
      @result = Loan.select("loans.year, SUM(loans.total_quantity) AS quantity").where(loan_date: date_ranges).order("quantity desc").group(:year).limit(5)
    end
  end

  def by_class
    if params[:commit].present? or params[:format] == "xlsx"
      start_date = "1-#{params[:month]}-#{params[:year]}".to_date
      end_date = start_date.end_of_month
      date_ranges = (start_date..end_date)
      @result = Loan.select("borrower_id, SUM(loans.total_quantity) AS quantity")
                  .where(loan_date: date_ranges, school_class: params[:class])
                  .order("quantity desc")
                  .group(:borrower_id)
                  .group_by(&:borrower_id)
                  .to_h

      @borrowers = Borrower.where(id: @result.keys)

      loan_books = LoanBook.joins(:book, :loan)
                    .where(loans: { loan_date: date_ranges, school_class: params[:class]})

      @result_books = loan_books
                        .select("loan_books.borrower_id, books.language, count(*)")
                        .group("loan_books.id, loan_books.borrower_id, books.language")
                        .group_by{|x| [x&.borrower_id, x&.language]}

      @languages = loan_books.collect{|x| x&.book&.language.to_s }.uniq.sort
    end
  end

  def detail
    if params[:format] == "xlsx"
      start_date = "1-#{params[:month]}-#{params[:year]}".to_date
      end_date = start_date.end_of_month
      date_ranges = (start_date..end_date)
      @loan_books = LoanBook.joins(:borrower, :book).select(%Q{
                      loan_books.school_class,
                      borrowers.name AS borrower_name,
                      books.name AS book_name,
                      books.barcode AS barcode,
                      loan_books.loan_date,
                      loan_books.due_date,
                      loan_books.return_date
                    }).where(school_class: params[:school_class], loan_date: date_ranges)
                    .order(:loan_date)
    end
  end
end
