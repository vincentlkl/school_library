class Settings::PenaltyCostsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_penalty_cost

  def show; end

  def create
    if @penalty_cost.update(penalty_cost_params)
      flash[:success] = "This penalty cost has been updated."
      redirect_to settings_penalty_costs_path
    else
      flash[:error] = "Sorry unable to update penalty cost due to #{@penalty_cost.errors.full_messages.to_sentence}"
      render :new
    end
  end

  private

  def penalty_cost_params
    params.require(:penalty_cost).permit(%I[id days penalty_cost])
  end

  def set_penalty_cost
    @penalty_cost = PenaltyCost.first
  end
end
