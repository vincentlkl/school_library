class StudentsDatatable
  delegate :params, :h, :link_to, :edit_users_student_path, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: Borrower.students.count,
      iTotalDisplayRecords: students.total_entries,
      aaData: data
    }
  end

private

  def data
    students.map do |student|
      [
        student.id,
        student.code,
        student.name,
        student.year,
        (link_to('Edit', edit_users_student_path(student), class: 'btn btn-warning')),
      ]
    end
  end

  def students
    @students ||= fetch_students
  end

  def fetch_students
    students = Borrower.students.order("#{sort_column} #{sort_direction}")
    students = students.page(page).per_page(per_page)
    if params[:sSearch].present?
      students = students.where("borrowers.name ilike :search OR
                          borrowers.code ilike :search
                        ", search: "%#{params[:sSearch]}%")
    end
    students.distinct
  end

  def page
    params[:iDisplayStart].to_i/per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = {
      '0': "borrowers.id",
      '1': "borrowers.code",
      '2': "borrowers.name",
      '3': "borrowers.year",
      '4': "borrowers.id",
    }
    columns[params[:iSortCol_0].to_sym]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc" : "asc"
  end
end
