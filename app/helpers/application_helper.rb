# frozen_string_literal: true

module ApplicationHelper
  BOOTSTRAP_ALERT_CLASSES = {
    success: "alert alert-success",
    error: "alert alert-danger",
    alert: "alert alert-warning",
    notice: "alert alert-info"
  }.freeze

  def flash_class(flash_type)
    BOOTSTRAP_ALERT_CLASSES[flash_type.to_sym]
  end

  def school_name
    "School Library"
  end

  def penalty_cost
    PenaltyCost.first.penalty_cost
  end

  def due_day
    PenaltyCost.first.days
  end

  def school_class
    Borrower.pluck(:school_class).reject(&:blank?).uniq.sort
  end
end