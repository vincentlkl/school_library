class WriteOffsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_write_off, only: %I[show]

  def index
    respond_to do |format|
      format.html
      format.json { render json: WriteOffsDatatable.new(view_context) }
    end
  end

  def new
    @write_off = WriteOff.new
    @write_off.write_off_date = Time.zone.today
  end

  def show; end

  def create
    @write_off = WriteOff.new(write_off_params)
    set_write_off_books
    if @write_off.save
      flash[:success] = "This write off has been created."
      redirect_to write_off_path(@write_off)
    else
      flash[:error] = "Sorry unable to create book due to #{@write_off.errors.full_messages.to_sentence}"
      render :new
    end
  end

  def add_book
    @book = Book.find_by(barcode: params[:barcode])
  end

  private

  def write_off_params
    params.require(:write_off).permit([:write_off_date, :remark, write_off_books_attributes: [:id, :book_id]])
  end

  def set_write_off
    @write_off = WriteOff.find(params[:id])
  end

  def set_write_off_books
    @write_off.write_off_books.each do |write_off_book|
      write_off_book.user_id = current_user.id
      write_off_book.price = write_off_book&.book&.price
    end
    @write_off.user_id = current_user.id
    @write_off.total_quantity = @write_off.write_off_books.size
    @write_off.total_price = @write_off.write_off_books.collect{|x| x.price}.sum
  end
end
