class CreateWriteOffBooks < ActiveRecord::Migration[5.2]
  def change
    create_table :write_off_books do |t|
      t.references :user, index: true
      t.references :write_off, index: true
      t.references :book, index: true
      t.monetize :price
      t.timestamps
    end
  end
end
