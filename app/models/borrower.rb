class Borrower < ApplicationRecord
  has_many :loan_books, dependent: :destroy

  scope :students, -> { where(role: "student") }
  scope :teachers, -> { where(role: "teacher") }


  class << self
    def import_students(file)
      spreadsheet = open_spreadsheet(file)
      header = ["code", "name", "year", "school_class"]
      (2..spreadsheet.last_row).map do |i|
        row = Hash[[header, spreadsheet.row(i)].transpose]
        borrower = Borrower.where(role: "student", code: row["code"]).first_or_initialize
        borrower.update(row)
      end
    end

    def import_teachers(file)
      spreadsheet = open_spreadsheet(file)
      header = ["code", "name"]
      (2..spreadsheet.last_row).map do |i|
        row = Hash[[header, spreadsheet.row(i)].transpose]
        borrower = Borrower.where(role: "teacher", code: row["code"]).first_or_initialize
        borrower.update(row)
      end
    end

    def open_spreadsheet(file)
      case File.extname(file.original_filename)
      when ".xlsx" then Roo::Excelx.new(file.path)
      else raise "Unknown file type: #{file.original_filename}"
      end
    end
  end
end
