class BooksController < ApplicationController
  before_action :authenticate_user!
  before_action :set_book, only: %I[edit update]
  
  def index
    respond_to do |format|
      format.html
      format.json { render json: BooksDatatable.new(view_context) }
      format.xlsx { @book_barcodes = Book.active.where.not(barcode: nil) }      
      format.pdf do 
        require 'chunky_png'
        require 'barby'
        require 'barby/barcode/code_128'    
        require 'barby/outputter/png_outputter'
        @books = Book.active.where.not(barcode: nil, barcode: "")
        if params[:barcodes].present?
          @books = @books.where(barcode: params[:barcodes].split(","))
        end
        render pdf: "index",
          layout: false, 
          page_size: 'A4',
          encoding: "utf-8",
          :margin => {top: 6, bottom: 6, left: 0, right: 0}
      end
      format.docx do
        @book_barcodes = Book.active.where.not(barcode: nil).pluck(:barcode).each_slice(3).to_a
        headers["Content-Disposition"] = "attachment; filename=\"books.docx\""
      end
    end
  end

  def new
    @book = Book.new
  end

  def edit; end

  def create
    @book = Book.new(book_params)
    if @book.save
      flash[:success] = "This books has been created."
      redirect_to books_path
    else
      flash[:error] = "Sorry unable to create book due to #{@book.errors.full_messages.to_sentence}"
      render :new
    end
  end

  def update
    if @book.update(book_params)
      flash[:success] = "This books has been updated."
      redirect_to books_path
    else
      flash[:error] = "Sorry unable to update book due to #{@book.errors.full_messages.to_sentence}"
      render :new
    end
  end

  def import
    @books = Book.import(params[:file])
    flash[:success] = "This books has been imported."
    redirect_to books_path
  end

  private

  def book_params
    params.require(:book).permit(%I[code name year language author description barcode genre price rack])
  end

  def set_book
    @book = Book.find(params[:id])
  end
end
