class ReturnsDatatable
  delegate :params, :h, :link_to, :return_path, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: Return.count,
      iTotalDisplayRecords: returns.total_entries,
      aaData: data
    }
  end

private

  def data
    returns.map do |return_record|
      [
        return_record.id,
        return_record&.borrower&.name,
        (return_record.return_date.to_date rescue "-"),
        (link_to('Show', return_path(return_record), class: 'btn btn-warning')),
      ]
    end
  end

  def returns
    @returns ||= fetch_returns
  end

  def fetch_returns
    returns = Return.joins(:borrower).order("#{sort_column} #{sort_direction}")
    returns = returns.page(page).per_page(per_page)
    if params[:sSearch].present?
      returns = returns.where("users.name ilike :search
                        ", search: "%#{params[:sSearch]}%")
    end
    returns.distinct
  end

  def page
    params[:iDisplayStart].to_i/per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = {
      '0': "returns.id",
      '1': "borrowers.name",
      '2': "returns.return_date",
      '3': "returns.id",
    }
    columns[params[:iSortCol_0].to_sym]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc" : "asc"
  end
end
