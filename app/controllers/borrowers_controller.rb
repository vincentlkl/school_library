class BorrowersController < ApplicationController
  before_action :authenticate_user!

  def ajax_select2
    respond_to do |format|
      format.json {
        render json: Borrower.where("
                          code ILIKE :search OR name ILIKE :search",
                          search: "%#{params[:q]}%")
                        .order("name")
                        .map{|borrower| { id: borrower.id, text: "#{borrower.code} (#{borrower.name})" }}
      }
    end
  end
end
