class CreateStudents < ActiveRecord::Migration[5.2]
  def change
    create_table :students do |t|
      t.string :code
      t.string :name
      t.string :year
      t.text :remark
      t.timestamps
    end
  end
end
