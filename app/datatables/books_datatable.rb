class BooksDatatable
  delegate :params, :h, :link_to, :edit_book_path, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: Book.active.count,
      iTotalDisplayRecords: books.total_entries,
      aaData: data
    }
  end

private

  def data
    books.map do |book|
      [
        book.id,
        book.code,
        book.name,
        book.year,
        book.author,
        book.barcode,
        (link_to('Edit', edit_book_path(book), class: 'btn btn-warning')),
      ]
    end
  end

  def books
    @books ||= fetch_books
  end

  def fetch_books
    books = Book.active.order("#{sort_column} #{sort_direction}")
    books = books.page(page).per_page(per_page)
    if params[:sSearch].present?
      books = books.where("books.name ilike :search OR
                          books.code ilike :search OR
                          books.author ilike :search OR
                          books.barcode ilike :search
                        ", search: "%#{params[:sSearch]}%")
    end
    books.distinct
  end

  def page
    params[:iDisplayStart].to_i/per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = {
      '0': "books.id",
      '1': "books.code",
      '2': "books.name",
      '3': "books.year",
      '4': "books.author",
      '5': "books.barcode",
      '6': "books.id",
    }
    columns[params[:iSortCol_0].to_sym]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc" : "asc"
  end
end
