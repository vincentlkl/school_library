class CreateBorrowers < ActiveRecord::Migration[5.2]
  def change
    create_table :borrowers do |t|
      t.string :code
      t.string :name
      t.string :year
      t.text :remark
      t.string :role, default: "student"
      t.timestamps
    end
  end
end
