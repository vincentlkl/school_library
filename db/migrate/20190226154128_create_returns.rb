class CreateReturns < ActiveRecord::Migration[5.2]
  def change
    create_table :returns do |t|
      t.references :borrower, index: true
      t.date :return_date
      t.text :remark
      t.integer :total_quantity
      t.timestamps
    end
  end
end
