class Return < ApplicationRecord
  belongs_to :borrower
  has_many :loan_books

  accepts_nested_attributes_for :loan_books, allow_destroy: true
end
